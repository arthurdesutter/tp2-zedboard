#include "dma.hpp"

//----------------------------------------------------------------
// GROUPE 2
// Arthur DESUTTER
// Clement LARIVIERE
// Anthony Sarkis
//----------------------------------------------------------------

int main() {
    unsigned long int status;

    DirectMemoryAccess* DMA = new DirectMemoryAccess(0x40400000, 0x0e000000, 0x0f000000); //Déclaration de la DMA

    DMA->reset(); //Redémarre la DMA
    DMA->halt();  //Stop la DMA
    DMA->writeSourceByte(0);
    DMA->writeSourceByte(10);
    DMA->writeSourceByte(5);
    DMA->writeSourceByte(0);
    DMA->writeSourceString("hello"); //Ecriture source
    DMA->hexdumpSource(9); //dump as hexa



    DMA->setInterrupt(true, false, 0);
    DMA->ready();

    DMA->setDestinationAddress(0x0f000000);
    DMA->setSourceAddress(0x0e000000);
    DMA->setDestinationLength(5);
    DMA->setSourceLength(9);

    printf("Waiting for MM2S...\n");
    do {
        status = DMA->getMM2SStatus();
        DMA->dumpStatus(status);
    } while((!(status & 1<<12) || !(status & 1 << 1)));

    printf("Waiting for S2MM...\n");
    do {
        status = DMA->getS2MMStatus();
        DMA->dumpStatus(status);
    } while((!(status & 1<<12) || !(status & 1 << 1)));

    DMA->hexdumpDestination(5);

    return 0;
}
